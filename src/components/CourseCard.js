//bootstrap
import { Row, Col, Card, Button } from 'react-bootstrap'

export default function CourseCard(){
	return(
		<Row>
			<Col xs={12} md={12}>
				<Card>
				  <Card.Header>Front End Development</Card.Header>
				  <Card.Body>
				    <Card.Title>Description</Card.Title>
				    <Card.Text>
				      Front-end web development is the development of the graphical user interface of a website, through the use of HTML, CSS, and JavaScript, so that users can view and interact with that website.
				    </Card.Text>
				    <Card.Title><h4>Price:</h4></Card.Title>
				    <Card.Text>10,000</Card.Text>
				    <Button variant="primary">Go somewhere</Button>
				  </Card.Body>
				  <Card.Footer className="text-muted">Updated 2 days ago</Card.Footer>
				</Card>
			</Col>
		</Row>
		)
}